#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     redis
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/28
-------------------------------------------------
   修改描述-2021/7/28:         
-------------------------------------------------
"""
from typing import Tuple

from aioredis import Redis

from apps.ext.cache.backends import Backend


class RedisBackend(Backend):
    def __init__(self, redis: Redis):
        self.redis = redis


    # async def get_with_ttl(self, key: str) -> Tuple[int, str]:
    #     print("赎回出生地2222", self.redis)
    #     self.redis.pipeline()
    #     print(2222222222)
    #         # return await (pipe.ttl(key).get(key).execute())

    # async def get_with_ttl(self, key: str) -> Tuple[int, str]:
    #     async with self.redis.pipeline() as pipe:
    #         return await (pipe.ttl(key).get(key).execute())

    async def get_with_ttl( self,key: str) -> Tuple[int, str]:
            pipe =  self.redis.pipeline()
            pipe.ttl(key)
            pipe.get(key)
            return await pipe.execute()

    async def get(self, key) -> str:
        return await self.redis.get(key)

    async def set(self, key: str, value: str, expire: int = None):
        return await self.redis.set(key, value, expire=expire)

    async def clear(self, namespace: str = None, key: str = None) -> int:
        if namespace:
            lua = f"for i, name in ipairs(redis.call('KEYS', '{namespace}:*')) do redis.call('DEL', name); end"
            return await self.redis.eval(lua, numkeys=0)
        elif key:
            return await self.redis.delete(key)


